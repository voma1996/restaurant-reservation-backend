FROM node:12-alpine

ARG NODE_ENV=development

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm ci

COPY . .

# RUN npx nest start --watch --debug=9999

EXPOSE 3000
EXPOSE 9999

CMD [ "npx", "nest", "start", "--watch", "--debug=9999" ]
