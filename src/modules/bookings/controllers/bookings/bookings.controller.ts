import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  NotFoundException,
  ParseIntPipe,
  Param,
  Patch,
  Delete,
} from '@nestjs/common';
import { DocumentType } from '@typegoose/typegoose';
import { ApiTags, ApiExtraModels } from '@nestjs/swagger';
import { Types } from 'mongoose';

import { IsObjectIdPipe } from '../../../common/pipes/is-object-id/is-object-id.pipe';
import { PlainToClassPipe } from '../../../common/pipes/plain-to-class/plain-to-class.pipe';
import { Tables } from '../../../tables/models/tables/tables.model';
import { FindTableByIdPipe } from '../../../tables/pipes/find-table-by-id/find-table-by-id.pipe';
import { CreateBookingDto } from '../../dtos/create-booking/create-booking.dto';
import { Bookings } from '../../models/bookings/bookings.model';
import { FindAllBookingsQueryDto } from '../../dtos/find-all-bookings-query/find-all-bookings-query.dto';
import { CloseBookingDto } from '../../dtos/close-booking/close-booking.dto';
import { BookingsService } from '../../services/bookings/bookings.service';
import { FindBookingByIdPipe } from '../../pipes/find-booking-by-id/find-booking-by-id.pipe';
import { TableIsBookedError } from '../../errors/table-is-booked/table-is-booked.error';
import { BookingIsClosedError } from '../../errors/booking-is-closed/booking-is-closed.error';
import { TableIsBookedException } from '../../exceptions/table-is-booked/table-is-booked.exception';
import { BookingIsClosedException } from '../../exceptions/booking-is-closed/booking-is-closed.excpetion';

@Controller('bookings')
@ApiExtraModels(Bookings)
@ApiTags('bookings')
export class BookingsController {
  constructor(public readonly bookingsService: BookingsService) {}

  @Post()
  async create(
    @Body() createBookingDto: CreateBookingDto,
    @Body('table', IsObjectIdPipe, FindTableByIdPipe) table: DocumentType<Tables>,
  ) {
    try {
      const booking = await this.bookingsService.create(createBookingDto, table);

      return {
        data: booking,
      };
    } catch (error) {
      if (error instanceof TableIsBookedError) {
        throw new TableIsBookedException(error);
      }

      throw error;
    }
  }

  @Get()
  async getAll(@Query(PlainToClassPipe) query: FindAllBookingsQueryDto) {
    const [allBookings, allCount] = await Promise.all([
      this.bookingsService.getAll(query),
      this.bookingsService.getAllCount(query),
    ]);

    const count = allBookings.length;

    return {
      data: allBookings,
      meta: {
        ...query,
        count,
        allCount,
      },
    };
  }

  @Get(':bookingId')
  async getOne(
    @Query() query: any,
    @Param('bookingId', IsObjectIdPipe) bookingId: Types.ObjectId,
    @Param('bookingId', IsObjectIdPipe, FindBookingByIdPipe) booking: DocumentType<Bookings>,
  ) {
    if (!booking) {
      throw new NotFoundException();
    }

    return {
      data: booking,
    };
  }

  @Patch(':bookingId')
  async update(
    @Param('bookingId', IsObjectIdPipe) bookingId: string,
    @Param('bookingId', IsObjectIdPipe, FindBookingByIdPipe) booking: DocumentType<Bookings>,
    @Body() updateBookingDto, // TODO: provide validation dto
  ) {
    try {
      if (!booking) {
        throw new NotFoundException();
      }

      const updatedBooking = await this.bookingsService.update(booking, updateBookingDto);

      return {
        data: updatedBooking,
      };
    } catch (error) {
      if (error instanceof BookingIsClosedError) {
        throw new BookingIsClosedException(error);
      }

      throw error;
    }
  }

  @Delete(':bookingId')
  async remove(
    @Param('bookingId', IsObjectIdPipe) bookingId: string,
    @Param('bookingId', IsObjectIdPipe, FindBookingByIdPipe) booking: DocumentType<Bookings>,
  ) {
    if (!booking) {
      throw new NotFoundException();
    }

    const removedBooking = await this.bookingsService.remove(booking);

    return {
      data: removedBooking,
    };
  }

  @Post(':bookingId/close')
  async close(
    @Param('bookingId', IsObjectIdPipe) bookingId: Types.ObjectId,
    @Param('bookingId', IsObjectIdPipe, FindBookingByIdPipe) booking: DocumentType<Bookings>,
    @Body() closeBookingDto: CloseBookingDto,
  ) {
    try {
      if (!booking) {
        throw new NotFoundException();
      }

      const updatedBooking = await this.bookingsService.close(booking, closeBookingDto);

      return {
        data: updatedBooking,
      };
    } catch (error) {
      if (error instanceof BookingIsClosedError) {
        throw new BookingIsClosedException(error);
      }

      throw error;
    }
  }
}
