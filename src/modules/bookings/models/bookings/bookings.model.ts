import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Typegoose, Ref, plugin, modelOptions, arrayProp, prop } from '@typegoose/typegoose';
import {
  IsString,
  IsArray,
  MinLength,
  IsOptional,
  IsInt,
  IsNumber,
  Matches,
  ValidateNested,
  IsDate,
  IsEnum,
  IsBoolean,
} from 'class-validator';

import { Tables } from '../../../tables/models/tables/tables.model';

@plugin(require('mongoose-autopopulate'))
@modelOptions({ schemaOptions: { timestamps: true } })
export class Bookings {
  @ApiProperty()
  @prop({ ref: 'Tables', required: true, autopopulate: true })
  table: Ref<Tables>;

  @ApiPropertyOptional()
  @IsString()
  @prop()
  name: string;

  @ApiProperty()
  @IsString()
  @prop()
  phoneNo: string;

  @ApiProperty()
  @IsDate()
  @prop({ required: true })
  from: Date;

  @ApiProperty()
  @IsDate()
  @prop()
  to: Date;

  @ApiProperty()
  @IsBoolean()
  @prop({ default: false })
  isClosedManually: boolean;
}
