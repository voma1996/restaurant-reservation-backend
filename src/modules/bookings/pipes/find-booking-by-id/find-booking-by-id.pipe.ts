import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { Types } from 'mongoose';

import { BookingsService } from '../../services/bookings/bookings.service';

@Injectable()
export class FindBookingByIdPipe implements PipeTransform {
  constructor(public readonly bookingsService: BookingsService) {}

  async transform(bookingId: string | Types.ObjectId, metadata: ArgumentMetadata) {
    const booking = await this.bookingsService.getById(bookingId);

    return booking;
  }
}
