import { FindBookingByIdPipe } from './find-booking-by-id.pipe';

describe('FindBookingByIdPipe', () => {
  it('should be defined', () => {
    expect(new FindBookingByIdPipe()).toBeDefined();
  });
});
