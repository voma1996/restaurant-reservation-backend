import { Injectable, Type, forwardRef, Inject } from '@nestjs/common';
import { ReturnModelType, DocumentType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { DocumentQuery, Types } from 'mongoose';
import * as moment from 'moment';

import { ConfigService } from '../../../config/services/config/config.service';
import { Tables } from '../../../tables/models/tables/tables.model';
import { Bookings } from '../../models/bookings/bookings.model';
import { CreateBookingDto } from '../../dtos/create-booking/create-booking.dto';
import { TableIsBookedError } from '../../errors/table-is-booked/table-is-booked.error';
import { BookingIsClosedError } from '../../errors/booking-is-closed/booking-is-closed.error';
import { FindAllParams } from '../../interfaces/find-all-params/find-all-params.interface';
import { findAllParamsDefault } from '../../constants/find-all-params/find-all-params.default.constant';

@Injectable()
export class BookingsService {
  constructor(
    @InjectModel(Bookings) public readonly bookingsModel: ReturnModelType<typeof Bookings>,
    public readonly configService: ConfigService,
  ) {}

  async create(createBookingDto: CreateBookingDto, table: DocumentType<Tables>) {
    const foundBooking = await this.findBookingByTableAndDate(table, createBookingDto.from);

    if (foundBooking) {
      throw new TableIsBookedError(table._id, createBookingDto.from);
    }

    const startAndEndOfWorkingDayFormat = this.configService.get('START_AND_END_OF_WORKING_DAY_FORMAT');
    const formattedEndOfWorkingDay = this.configService.get('FORMATTED_END_OF_WORKING_DAY');
    const endOfWorkingDayDate = moment(createBookingDto.from)
      .startOf('day')
      .add(moment(formattedEndOfWorkingDay, startAndEndOfWorkingDayFormat).diff(moment().startOf('day')))
      .toDate();

    const booking = await this.bookingsModel.create({
      to: endOfWorkingDayDate,
      ...createBookingDto,
    });

    return booking;
  }

  async getAll({
    sortBy = findAllParamsDefault.sortBy,
    sortDir = findAllParamsDefault.sortDir,
    skip = findAllParamsDefault.skip,
    limit = findAllParamsDefault.limit,
  }: FindAllParams = findAllParamsDefault) {
    const bookingsQuery = this.bookingsModel.find();

    if (sortBy && sortDir) {
      bookingsQuery.sort({ [sortDir]: sortDir.toUpperCase() === 'ASC' ? 1 : -1 });
    }

    if (skip) {
      bookingsQuery.skip(parseInt(skip as any, 10));
    }

    if (limit) {
      bookingsQuery.limit(parseInt(limit as any, 10));
    }

    const allBookings = await bookingsQuery.exec();

    return allBookings;
  }

  async getAllCount({}: FindAllParams = findAllParamsDefault) {
    const bookingsQuery = this.bookingsModel.find({});

    const allCount = await bookingsQuery.countDocuments().exec();

    return allCount;
  }

  async getById(bookingId: string | Types.ObjectId) {
    const booking = await this.bookingsModel.findById(bookingId).exec();

    return booking;
  }

  async findBookingByTableAndDate(table: DocumentType<Tables>, date: Date) {
    const from = moment(date)
      .startOf('day')
      .toDate();
    const to = moment(date)
      .endOf('day')
      .toDate();

    const foundBooking = await this.bookingsModel.findOne({
      $and: [
        { table: table._id },
        {
          $or: [
            {
              $and: [{ from: { $lte: moment(to).toDate() } }, { to: { $gte: moment(from).toDate() } }],
            },
            {
              $and: [{ from: { $lte: moment(to).toDate() } }, { to: null }],
            },
          ],
        },
      ],
    });

    return foundBooking;
  }

  async update(booking: DocumentType<Bookings>, updateBookingDto: any) {
    const updateBookigns = await this.updateById(booking._id, updateBookingDto);

    return updateBookigns;
  }

  async updateById(bookingId: string | Types.ObjectId, updateBookingDto) {
    const updatedBooking = await this.bookingsModel
      .findByIdAndUpdate(bookingId, { $set: { ...updateBookingDto, updatedAt: new Date() } }, { new: true })
      .exec();

    return updatedBooking;
  }

  async remove(booking: DocumentType<Bookings>, ...args: any[]) {
    const proms: Array<Promise<any>> = [this.removeById(booking._id)];

    const results = await Promise.all(proms);

    return booking;
  }

  async removeById(bookingId: string | Types.ObjectId) {
    const removedBooking = await this.bookingsModel.findByIdAndDelete(bookingId).exec();

    return removedBooking;
  }

  async close(booking: DocumentType<Bookings>, closeBookingDto) {
    if (booking.isClosedManually) {
      throw new BookingIsClosedError(booking._id, booking.to);
    }

    const updatedBooking = await this.updateById(booking._id, {
      ...closeBookingDto,
      isClosedManually: true,
    });

    return updatedBooking;
  }

  async getBookingsInRangeByTable(table: DocumentType<Tables>, from: Date | moment.Moment, to: Date | moment.Moment) {
    const bookingsInRange = await this.bookingsModel
      .find({
        $and: [
          { table: table._id },
          {
            $or: [
              {
                $and: [{ from: { $lte: moment(to).toDate() } }, { to: { $gte: moment(from).toDate() } }],
              },
              {
                $and: [{ from: { $lte: moment(to).toDate() } }, { to: null }],
              },
            ],
          },
        ],
      })
      .sort('from')
      .exec();

    return bookingsInRange;
  }

  async getBookingsInRangePerDayByTable(
    table: DocumentType<Tables>,
    from: Date | moment.Moment,
    to: Date | moment.Moment,
    { produceFakes = true } = { produceFakes: true },
  ) {
    const periodBookings: Array<{ _id: string; bookingsInRange: Bookings[] }> = await this.bookingsModel
      .aggregate([
        {
          $match: {
            $and: [
              { table: table._id },
              {
                $or: [
                  {
                    $and: [{ from: { $lte: moment(to).toDate() } }, { to: { $gte: moment(from).toDate() } }],
                  },
                  {
                    $and: [{ from: { $lte: moment(to).toDate() } }, { to: null }],
                  },
                ],
              },
            ],
          },
        },
        {
          $sort: { from: 1 },
        },
        {
          $addFields: {
            period: {
              $concat: [{ $substr: ['$from', 0, 10] }],
            },
          },
        },
        {
          $group: {
            _id: '$period',
            bookingsInRange: {
              $push: '$$ROOT',
            },
          },
        },
      ])
      .exec();

    if (produceFakes) {
      const fromStartOfDayMoment = moment(from).startOf('day');
      const toStartOfDayMoment = moment(to).startOf('day');

      let tempMomentDay = fromStartOfDayMoment;

      do {
        const tempMomentFormattedDate = tempMomentDay.format('YYYY-MM-DD');
        const foundPeriod = periodBookings.find(periodBooking => periodBooking._id === tempMomentFormattedDate);

        if (!foundPeriod) {
          const fakePeriodBookings = {
            _id: tempMomentFormattedDate,
            bookingsInRange: [],
          };

          periodBookings.push(fakePeriodBookings);
        }

        tempMomentDay = tempMomentDay.add(1, 'day');
      } while (!tempMomentDay.isAfter(toStartOfDayMoment));
    }

    periodBookings.sort(
      (left, right) =>
        moment(left._id, 'YYYY-MM-DD')
          .toDate()
          .getTime() -
        moment(right._id, 'YYYY-MM-DD')
          .toDate()
          .getTime(),
    );

    return periodBookings;
  }

  async removeBookingsByTableId(tableId: string | Types.ObjectId) {
    const relatedBookings = await this.bookingsModel.find({ table: tableId }).exec();

    const results = await Promise.all(relatedBookings.map(async booking => await this.removeById(booking._id)));

    return results;
  }

  async removeBookingsByTable(table: DocumentType<Tables>) {
    const relatedBookings = await this.bookingsModel.find({ table: table._id });

    const proms: Array<Promise<any>> = [];
  }
}
