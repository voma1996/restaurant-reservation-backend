import { Types } from 'mongoose';

export class TableIsBookedError extends Error {
  constructor(tableId: string | Types.ObjectId, date: Date) {
    super(`Table ${tableId} is booked at ${date.toISOString()}`);
  }
}
