import { Types } from 'mongoose';

export class BookingIsClosedError extends Error {
  constructor(bookingId: string | Types.ObjectId, date: Date) {
    super(`Booking ${bookingId} is closed at ${date.toISOString()}`);
  }
}
