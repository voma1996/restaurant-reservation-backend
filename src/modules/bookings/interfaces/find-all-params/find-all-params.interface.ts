export interface FindAllParams {
  sortBy?: string;
  sortDir?: string;
  skip?: number;
  limit?: number;
}
