import { HttpException, HttpStatus } from '@nestjs/common';

import { BookingIsClosedError } from '../../errors/booking-is-closed/booking-is-closed.error';

export class BookingIsClosedException extends HttpException {
  constructor(bookingIsBookedError: BookingIsClosedError) {
    super(bookingIsBookedError.message, HttpStatus.CONFLICT);
  }
}
