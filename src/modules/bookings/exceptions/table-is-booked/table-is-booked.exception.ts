import { HttpException, HttpStatus } from '@nestjs/common';

import { TableIsBookedError } from '../../errors/table-is-booked/table-is-booked.error';

export class TableIsBookedException extends HttpException {
  constructor(tableIsBookedError: TableIsBookedError) {
    super(tableIsBookedError.message, HttpStatus.CONFLICT);
  }
}
