import { FindAllParams } from '../../interfaces/find-all-params/find-all-params.interface';

export const findAllParamsDefault: FindAllParams = {
  sortBy: '_id',
  limit: 100,
  skip: 0,
};
