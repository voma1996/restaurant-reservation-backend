import { ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';
import { Type } from 'class-transformer';
import { IsNumber, IsString, IsOptional, IsDate } from 'class-validator';

export class CloseBookingDto {
  @IsDate()
  @IsOptional()
  @Type(() => Date)
  to: Date = new Date();
}
