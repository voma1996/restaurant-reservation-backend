import { ApiPropertyOptional } from '@nestjs/swagger';
import { Type, Transform } from 'class-transformer';
import { IsString, IsOptional, IsNumber, IsDate, Equals } from 'class-validator';

import { ToDate, Default } from '../../../common/class-transformer-utils';
import { FindAllParams } from '../../interfaces/find-all-params/find-all-params.interface';
import { findAllParamsDefault } from '../../constants/find-all-params/find-all-params.default.constant';

export class FindAllBookingsQueryDto implements FindAllParams {
  @ApiPropertyOptional({ default: findAllParamsDefault.sortBy })
  @IsString()
  @IsOptional()
  sortBy: string = findAllParamsDefault.sortBy;

  @ApiPropertyOptional({ default: findAllParamsDefault.sortDir })
  @IsString()
  @IsOptional()
  sortDir: string = findAllParamsDefault.sortDir;

  @ApiPropertyOptional({ default: findAllParamsDefault.skip })
  @Type(() => Date)
  @IsOptional()
  skip: number = findAllParamsDefault.skip;

  @ApiPropertyOptional({ default: findAllParamsDefault.limit })
  @Type(() => Date)
  @IsOptional()
  limit: number = findAllParamsDefault.limit;
}
