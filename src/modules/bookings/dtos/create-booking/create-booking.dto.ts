import { ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';
import { Type } from 'class-transformer';
import { IsNumber, IsString, IsOptional, IsDate, IsMongoId } from 'class-validator';

import { IsRealTableId } from '../../../tables/validators/is-real-table-id/is-real-table-id.validator';

export class CreateBookingDto {
  @IsMongoId()
  @IsRealTableId()
  table: Types.ObjectId;

  @IsString()
  @IsOptional()
  phoneNo: string;

  @IsDate()
  @Type(() => Date)
  from: Date = new Date();

  @IsDate()
  @IsOptional()
  @Type(() => Date)
  to: Date;
}
