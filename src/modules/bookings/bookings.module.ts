import { Module, forwardRef } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';

import { ConfigModule } from '../config/config.module';
import { CommonModule } from '../common/common.module';
import { TablesModule } from '../tables/tables.module';
import { Bookings } from './models/bookings/bookings.model';
import { BookingsService } from './services/bookings/bookings.service';
import { BookingsController } from './controllers/bookings/bookings.controller';

@Module({
  imports: [TypegooseModule.forFeature([Bookings]), ConfigModule, CommonModule, forwardRef(() => TablesModule)],
  providers: [BookingsService],
  controllers: [BookingsController],
  exports: [TypegooseModule, BookingsService],
})
export class BookingsModule {}
