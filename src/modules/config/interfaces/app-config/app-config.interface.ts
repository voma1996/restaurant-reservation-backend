export interface IAppConfig {
  PORT: string | number;

  START_AND_END_OF_WORKING_DAY_FORMAT: string;
  FORMATTED_START_OF_WORKING_DAY: string;
  FORMATTED_END_OF_WORKING_DAY: string;
}
