import { Module } from '@nestjs/common';
import { configServiceProvider } from './providers/config-service/config-service.provider';
import { ConfigService } from './services/config/config.service';
import { ConfigController } from './controllers/config/config.controller';

@Module({
  providers: [configServiceProvider],
  controllers: [ConfigController],
  exports: [ConfigService],
})
export class ConfigModule {}
