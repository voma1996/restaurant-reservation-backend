import { IAppConfig } from '../../interfaces/app-config/app-config.interface';

export const appConfigDefault: IAppConfig = {
  PORT: 3001,

  START_AND_END_OF_WORKING_DAY_FORMAT: 'HH:mm',
  FORMATTED_START_OF_WORKING_DAY: '08:00',
  FORMATTED_END_OF_WORKING_DAY: '23:59',
};
