import { Controller, Get } from '@nestjs/common';

import { ConfigService } from '../../services/config/config.service';

@Controller('config')
export class ConfigController {
  constructor(public readonly configService: ConfigService) {}

  @Get('app')
  async getPublicAppConfig() {
    const START_AND_END_OF_WORKING_DAY_FORMAT = this.configService.get('START_AND_END_OF_WORKING_DAY_FORMAT');
    const FORMATTED_START_OF_WORKING_DAY = this.configService.get('FORMATTED_START_OF_WORKING_DAY');
    const FORMATTED_END_OF_WORKING_DAY = this.configService.get('FORMATTED_END_OF_WORKING_DAY');

    return {
      START_AND_END_OF_WORKING_DAY_FORMAT,
      FORMATTED_START_OF_WORKING_DAY,
      FORMATTED_END_OF_WORKING_DAY,
    };
  }
}
