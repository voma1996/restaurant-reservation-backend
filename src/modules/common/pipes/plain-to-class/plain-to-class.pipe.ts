import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException, Optional } from '@nestjs/common';
import { plainToClass } from 'class-transformer';

@Injectable()
export class PlainToClassPipe implements PipeTransform {
  constructor(@Optional() public readonly cls: any) {}

  async transform(value: string, metadata: ArgumentMetadata) {
    const cls = this.cls || metadata.metatype;

    if (cls) {
      const classified = await plainToClass(cls, value);

      return classified;
    }

    return value;
  }
}
