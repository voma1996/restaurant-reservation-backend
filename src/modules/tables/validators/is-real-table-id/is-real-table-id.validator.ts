import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';
import { getModelForClass } from '@typegoose/typegoose';
import { Types } from 'mongoose';

import { Tables } from '../../models/tables/tables.model';

export function IsRealTableId(
  property?: string,
  validationOptions: ValidationOptions = { message: 'have to be valid tableId' },
) {
  return function(object: Object, propertyName: string) {
    registerDecorator({
      name: 'isRealTableId',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      async: true,
      validator: {
        async validate(value: any, args: ValidationArguments) {
          const tablesModel = getModelForClass(Tables);

          const isObjectId = Types.ObjectId.isValid(value);

          if (!isObjectId) {
            return false;
          }

          const foundTable = await tablesModel
            .findById(value)
            .countDocuments()
            .exec();

          return !!foundTable;
        },
      },
    });
  };
}
