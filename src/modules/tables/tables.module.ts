import { Module, forwardRef } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';

import { ConfigModule } from '../config/config.module';
import { CommonModule } from '../common/common.module';
import { BookingsModule } from '../bookings/bookings.module';
import { Tables } from './models/tables/tables.model';
import { TablesService } from './services/tables/tables.service';
import { FindTableByIdPipe } from './pipes/find-table-by-id/find-table-by-id.pipe';
import { TablesController } from './controllers/tables/tables.controller';

@Module({
  imports: [TypegooseModule.forFeature([Tables]), ConfigModule, CommonModule, forwardRef(() => BookingsModule)],
  providers: [TablesService, FindTableByIdPipe],
  controllers: [TablesController],
  exports: [TypegooseModule, TablesService, FindTableByIdPipe],
})
export class TablesModule {}
