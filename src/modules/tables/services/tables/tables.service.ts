import { Injectable, Type, forwardRef, Inject } from '@nestjs/common';
import { ReturnModelType, DocumentType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { DocumentQuery, Types } from 'mongoose';
import * as moment from 'moment';

import { ConfigService } from '../../../config/services/config/config.service';
import { BookingsService } from '../../../bookings/services/bookings/bookings.service';
import { Tables } from '../../models/tables/tables.model';
import { CreateTableDto } from '../../dtos/create-table/create-table.dto';
import { TableStates } from '../../enums/table-states/table-states.enum';
import { FindAllParams } from '../../interfaces/find-all-params/find-all-params.interface';
import { findAllParamsDefault } from '../../constants/find-all-params/find-all-params.default.constant';

@Injectable()
export class TablesService {
  constructor(
    @InjectModel(Tables) public readonly tablesModel: ReturnModelType<typeof Tables>,
    @Inject(forwardRef(() => BookingsService)) public readonly bookingsService: BookingsService,
    public readonly configService: ConfigService,
  ) {}

  async create(createTableDto: CreateTableDto) {
    const startAndEndOfWorkingDayFormat = this.configService.get('START_AND_END_OF_WORKING_DAY_FORMAT');
    const formattedEndOfWorkingDay = this.configService.get('FORMATTED_END_OF_WORKING_DAY');

    const table = await this.tablesModel.create(createTableDto);

    return table;
  }

  async getAll({
    sortBy = findAllParamsDefault.sortBy,
    sortDir = findAllParamsDefault.sortDir,
    skip = findAllParamsDefault.skip,
    limit = findAllParamsDefault.limit,
    resolveState = findAllParamsDefault.resolveState,
    date = new Date(),
    resolveAvailableRanges = findAllParamsDefault.resolveAvailableRanges,
    from = date || new Date(),
    to = moment()
      .endOf('day')
      .toDate(),
  }: FindAllParams = findAllParamsDefault) {
    const tablesQuery = this.tablesModel.find();

    if (sortBy && sortDir) {
      tablesQuery.sort({ [sortDir]: sortDir.toUpperCase() === 'ASC' ? 1 : -1 });
    }

    if (skip) {
      tablesQuery.skip(parseInt(skip as any, 10));
    }

    if (limit) {
      tablesQuery.limit(parseInt(limit as any, 10));
    }

    const allTables = await tablesQuery.exec();

    if (resolveState) {
      await this.assignTablesState(allTables, date);
    }

    if (resolveAvailableRanges && from && to) {
      await Promise.all(
        allTables.map(async table => {
          const availableRanges = await this.getAvailableTimeRanges(table, from, to);

          table.availableRanges = availableRanges;

          return table;
        }),
      );
    }

    return allTables;
  }

  async getAllCount({}: FindAllParams = findAllParamsDefault) {
    const tablesQuery = this.tablesModel.find({});

    const allCount = await tablesQuery.countDocuments().exec();

    return allCount;
  }

  async getById(tableId: string | Types.ObjectId) {
    const table = await this.tablesModel.findById(tableId);

    return table;
  }

  async assignTablesState(tables: Array<DocumentType<Tables>>, date: Date) {
    await Promise.all(tables.map(async table => this.assignTableState(table, date)));

    return tables;
  }

  async assignTableState(table: DocumentType<Tables>, date: Date) {
    const tableState = await this.resolveTableState(table, date);

    table.state = tableState;

    return table;
  }

  async resolveTablesState(tables: Array<DocumentType<Tables>>, date: Date) {
    const states = await Promise.all(tables.map(async table => await this.resolveTableState(table, date)));

    return states;
  }

  async resolveTableState(table: DocumentType<Tables>, date: Date) {
    const foundBooking = await this.bookingsService.findBookingByTableAndDate(table, date);

    const tableState = foundBooking ? TableStates.BOOKED : TableStates.FREE;

    return tableState;
  }

  async update(table: DocumentType<Tables>, updateTableDto: any) {
    const updatedTable = this.updateById(table._id, updateTableDto);

    return updatedTable;
  }

  async updateById(tableId: string | Types.ObjectId, updateTableDto: any) {
    const updatedTable = await this.tablesModel.findByIdAndUpdate(tableId, updateTableDto, { new: true }).exec();

    return updatedTable;
  }

  async remove(table: DocumentType<Tables>, ...args: any[]) {
    const proms: Array<Promise<any>> = [this.removeById(table._id)];

    proms.push(this.bookingsService.removeBookingsByTable(table));

    const results = await Promise.all(proms);

    return table;
  }

  async removeById(tableId: string | Types.ObjectId) {
    return await await this.tablesModel.findByIdAndDelete(tableId).exec();
  }

  async getAvailableTimeRanges(table: DocumentType<Tables>, from: Date | moment.Moment, to: Date | moment.Moment) {
    const bookingsInRangePerDay = await this.bookingsService.getBookingsInRangePerDayByTable(table, from, to);

    const startAndEndOfWorkingDayFormat = this.configService.get('START_AND_END_OF_WORKING_DAY_FORMAT');
    const formattedStartOfWorkingDay = this.configService.get('FORMATTED_START_OF_WORKING_DAY');
    const formattedEndOfWorkingDay = this.configService.get('FORMATTED_END_OF_WORKING_DAY');

    const results = bookingsInRangePerDay.map(bookingsDay => {
      const availableTimeRangesPerDay: Array<{ from: Date; to: Date }> = [];

      const startOfWorkingDayDate = moment(bookingsDay._id, 'YYYY-MM-DD')
        .startOf('day')
        .add(moment(formattedStartOfWorkingDay, startAndEndOfWorkingDayFormat).diff(moment().startOf('day')))
        .toDate();

      // const endOfWorkingDayDate = moment(formattedEndOfWorkingDay, startAndEndOfWorkingDayFormat).toDate();
      const endOfWorkingDayDate = moment(bookingsDay._id, 'YYYY-MM-DD')
        .startOf('day')
        .add(moment(formattedEndOfWorkingDay, startAndEndOfWorkingDayFormat).diff(moment().startOf('day')))
        .toDate();

      let virtualStartDate = moment(startOfWorkingDayDate).isBefore(from)
        ? moment(from).toDate()
        : moment(startOfWorkingDayDate).toDate();

      const bookingsInRange = bookingsDay.bookingsInRange;

      if (!bookingsInRange.length) {
        availableTimeRangesPerDay.push({ from: virtualStartDate, to: endOfWorkingDayDate });
      } else if (bookingsInRange.length === 1) {
        const singleBooking = bookingsInRange[0];

        virtualStartDate = moment(virtualStartDate).isSameOrBefore(singleBooking.from)
          ? virtualStartDate
          : singleBooking.from;

        if (singleBooking.from.getTime() !== virtualStartDate.getTime()) {
          availableTimeRangesPerDay.push({ from: virtualStartDate, to: singleBooking.from });
        }

        if (singleBooking.to && singleBooking.to.getTime() !== endOfWorkingDayDate.getTime()) {
          availableTimeRangesPerDay.push({ from: singleBooking.to, to: endOfWorkingDayDate });
        }
      } else if (bookingsInRange.length >= 2) {
        for (let i = 0; i < bookingsInRange.length - 1; i++) {
          const bookingInRange = bookingsInRange[i];
          const nextBookingInRange = bookingsInRange[i + 1];

          if (!nextBookingInRange.to) {
            break;
          }

          const availableTimeRange = { from: bookingInRange.to, to: nextBookingInRange.from };

          availableTimeRangesPerDay.push(availableTimeRange);
        }

        virtualStartDate = moment(virtualStartDate).isSameOrBefore(bookingsInRange[0].from)
          ? virtualStartDate
          : bookingsInRange[0].from;

        if (virtualStartDate.getTime() !== bookingsInRange[0].from.getTime()) {
          availableTimeRangesPerDay.unshift({ from: virtualStartDate, to: bookingsInRange[0].from });
        }

        if (
          bookingsInRange[bookingsInRange.length - 1].to &&
          endOfWorkingDayDate.getTime() !== bookingsInRange[bookingsInRange.length - 1].to.getTime()
        ) {
          availableTimeRangesPerDay.push({
            from: bookingsInRange[bookingsInRange.length - 1].to,
            to: endOfWorkingDayDate,
          });
        }
      }

      return { availableTimeRangesPerDay };
    });

    const availableTimeRanges: Array<{ from: Date; to: Date }> = results
      .map(result => result.availableTimeRangesPerDay)
      .reduce((accumulator, availableTimeRangesPerDay) => {
        accumulator.push(...availableTimeRangesPerDay);
        return accumulator;
      }, []);

    return availableTimeRanges;
  }
}
