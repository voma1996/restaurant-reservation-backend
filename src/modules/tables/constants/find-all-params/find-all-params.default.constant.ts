import { FindAllParams } from '../../interfaces/find-all-params/find-all-params.interface';

export const findAllParamsDefault: FindAllParams = {
  sortBy: '_id',
  sortDir: 'ASC',
  limit: 100,
  skip: 0,
  resolveState: 1,
  resolveAvailableRanges: 1,
};
