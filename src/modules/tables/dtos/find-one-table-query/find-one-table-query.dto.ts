import { Type, Transform } from 'class-transformer';
import { IsString, IsDateString, IsOptional, IsNumber, IsBoolean, IsBooleanString } from 'class-validator';

import { ToDate, Default } from '../../../common/class-transformer-utils';
import { FindAllParams } from '../../interfaces/find-all-params/find-all-params.interface';
import { findAllParamsDefault } from '../../constants/find-all-params/find-all-params.default.constant';

export class FindOneTableQueryDto implements FindAllParams {
  @IsBooleanString()
  @IsOptional()
  @Default(findAllParamsDefault.resolveState)
  resolveState: number;

  @IsDateString()
  @IsOptional()
  @ToDate(() => new Date())
  date: Date;
}
