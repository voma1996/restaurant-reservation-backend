import { ApiPropertyOptional } from '@nestjs/swagger';
import { Type, Transform } from 'class-transformer';
import { IsString, IsOptional, IsNumber, IsDate, Equals } from 'class-validator';
import * as moment from 'moment';

import { ToDate, Default } from '../../../common/class-transformer-utils';
import { FindAllParams } from '../../interfaces/find-all-params/find-all-params.interface';
import { findAllParamsDefault } from '../../constants/find-all-params/find-all-params.default.constant';

export class FindAllTablesQueryDto implements FindAllParams {
  @ApiPropertyOptional({ default: findAllParamsDefault.sortBy })
  @IsString()
  @IsOptional()
  sortBy: string = findAllParamsDefault.sortBy;

  @ApiPropertyOptional({ default: findAllParamsDefault.sortDir })
  @IsString()
  @IsOptional()
  sortDir: string = findAllParamsDefault.sortDir;

  @ApiPropertyOptional({ default: findAllParamsDefault.skip })
  @IsOptional()
  @Type(() => Number)
  skip: number = findAllParamsDefault.skip;

  @ApiPropertyOptional({ default: findAllParamsDefault.limit })
  @IsOptional()
  @Type(() => Number)
  limit: number = findAllParamsDefault.limit;

  @ApiPropertyOptional({ default: findAllParamsDefault.resolveState })
  @IsOptional()
  @Type(() => Number)
  resolveState: number = findAllParamsDefault.resolveState;

  @ApiPropertyOptional({})
  @IsDate()
  @IsOptional()
  @Type(() => Date)
  date: Date = new Date();

  @ApiPropertyOptional({ default: findAllParamsDefault.resolveAvailableRanges })
  @IsOptional()
  @Type(() => Number)
  resolveAvailableRanges: number = findAllParamsDefault.resolveAvailableRanges;

  @ApiPropertyOptional({})
  @IsDate()
  @IsOptional()
  @Type(() => Date)
  from: Date; /* = this.date || new Date(); */

  @ApiPropertyOptional({})
  @IsDate()
  @IsOptional()
  @Type(() => Date)
  to: Date; /* = moment()
    .endOf('day')
    .toDate(); */
}
