export interface FindAllParams {
  sortBy?: string;
  sortDir?: string;
  skip?: number;
  limit?: number;
  resolveState?: number;
  date?: Date;
  resolveAvailableRanges?: number;
  from?: Date;
  to?: Date;
}
