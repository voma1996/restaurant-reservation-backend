export interface FindAllParams {
  resolveState?: boolean;
  date?: Date;
}
