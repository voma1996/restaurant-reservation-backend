import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { Types } from 'mongoose';

import { TablesService } from '../../services/tables/tables.service';

@Injectable()
export class FindTableByIdPipe implements PipeTransform {
  constructor(public readonly tablesService: TablesService) {}

  async transform(tableId: string | Types.ObjectId, metadata: ArgumentMetadata) {
    const table = await this.tablesService.getById(tableId);

    return table;
  }
}
