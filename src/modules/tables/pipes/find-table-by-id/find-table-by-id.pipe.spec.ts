import { FindTableByIdPipe } from './find-table-by-id.pipe';

describe('FindTableByIdPipe', () => {
  it('should be defined', () => {
    expect(new FindTableByIdPipe()).toBeDefined();
  });
});
