import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  NotFoundException,
  ParseIntPipe,
  Param,
  Patch,
  Delete,
} from '@nestjs/common';
import { DocumentType } from '@typegoose/typegoose';
import { ApiTags, ApiExtraModels } from '@nestjs/swagger';
import { Types } from 'mongoose';
import * as moment from 'moment';

import { IsObjectIdPipe } from '../../../common/pipes/is-object-id/is-object-id.pipe';
import { PlainToClassPipe } from '../../../common/pipes/plain-to-class/plain-to-class.pipe';
import { CreateTableDto } from '../../dtos/create-table/create-table.dto';
import { Tables } from '../../models/tables/tables.model';
import { FindAllTablesQueryDto } from '../../dtos/find-all-tables-query/find-all-tables-query.dto';
import { FindTableByIdPipe } from '../../pipes/find-table-by-id/find-table-by-id.pipe';
import { TablesService } from '../../services/tables/tables.service';

@Controller('tables')
@ApiExtraModels(Tables)
@ApiTags('tables')
export class TablesController {
  constructor(public readonly tablesService: TablesService) {}

  @Post()
  async create(@Body() createTableDto: CreateTableDto) {
    const table = await this.tablesService.create(createTableDto);

    return {
      data: table,
    };
  }

  @Get()
  async getAll(@Query(PlainToClassPipe) query: FindAllTablesQueryDto) {
    query.from = query.from || query.date;
    query.to = query.to
      ? moment(query.to).isAfter(moment(query.from))
        ? query.to
        : moment(query.from)
            .endOf('day')
            .toDate()
      : moment(query.from)
          .endOf('day')
          .toDate();

    const [allTables, allCount] = await Promise.all([
      this.tablesService.getAll(query),
      this.tablesService.getAllCount(query),
    ]);

    const count = allTables.length;

    return {
      data: allTables,
      meta: {
        ...query,
        count,
        allCount,
      },
    };
  }

  @Get(':tableId')
  async getOne(
    @Query() query: any,
    @Param('tableId', IsObjectIdPipe) tableId: Types.ObjectId,
    @Param('tableId', IsObjectIdPipe, FindTableByIdPipe) table: DocumentType<Tables>,
  ) {
    if (!table) {
      throw new NotFoundException();
    }

    return {
      data: table,
    };
  }

  @Patch(':tableId')
  async update(
    @Param('tableId', IsObjectIdPipe) tableId: string,
    @Param('tableId', IsObjectIdPipe, FindTableByIdPipe) table: DocumentType<Tables>,
    @Body() updateTableDto, // TODO: provide validation dto
  ) {
    if (!table) {
      throw new NotFoundException();
    }

    const updatedTable = await this.tablesService.update(table, updateTableDto);

    return {
      data: updatedTable,
    };
  }

  @Delete(':tableId')
  async remove(
    @Param('tableId', IsObjectIdPipe) tableId: string,
    @Param('tableId', IsObjectIdPipe, FindTableByIdPipe) table: DocumentType<Tables>,
  ) {
    if (!table) {
      throw new NotFoundException();
    }

    const removedTable = await this.tablesService.remove(table);

    return {
      data: removedTable,
    };
  }

  @Get(':tableId/available-time-ranges')
  async getAvailableTimeRanges(
    @Param('tableId', IsObjectIdPipe) tableId: string,
    @Param('tableId', IsObjectIdPipe, FindTableByIdPipe) table: DocumentType<Tables>,
    @Param('from') from: Date | moment.Moment = moment().startOf('day'),
    @Param('to') to: Date | moment.Moment = moment().endOf('day'),
  ) {
    if (!table) {
      throw new NotFoundException();
    }

    const availableTimeRanges = await this.tablesService.getAvailableTimeRanges(table, from, to);

    return {
      data: availableTimeRanges,
    };
  }
}
