import { Typegoose, Ref, modelOptions, arrayProp, prop } from '@typegoose/typegoose';
import {
  IsString,
  IsArray,
  MinLength,
  IsOptional,
  IsInt,
  IsNumber,
  Matches,
  ValidateNested,
  IsDate,
  IsEnum,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { TableStates } from '../../enums/table-states/table-states.enum';

@modelOptions({ schemaOptions: { timestamps: true } })
export class Tables {
  @ApiPropertyOptional()
  @IsString()
  @prop()
  name: string;

  @ApiProperty()
  @IsNumber()
  @prop()
  places: number;

  @ApiPropertyOptional()
  @IsEnum(TableStates)
  @prop({ default: undefined })
  state?: TableStates;

  @ApiPropertyOptional()
  @prop({ default: undefined })
  availableRanges?: Array<{ from: Date; to: Date }>;
}
